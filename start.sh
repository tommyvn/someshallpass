#!/bin/bash
# see scripts/debian-init.d for production deployments

export PYTHONPATH=`dirname $0`

. ${PYTHONPATH}/conf/server.conf

twistd -n cyclone -p $PORT -l $ADDRESS \
       -r someshallpass.web.Application -c ${PYTHONPATH}/someshallpass.conf $*
