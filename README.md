## Some Shall Pass

**A simple feature service inspired by Facebook's Gatekeeper service, as described briefly [here](http://www.facebook.com/video/video.php?v=10100259101684977).**

This of course IS NOT gatekeeper, it's almost certainly a very different system and even more certainly less feature-complete.

The idea goes like this:

1. A feature is a simple boolean expression, say the NewHomePage feature wants 'userId in QA or userId in betaTesters and country == "UK"'. There is very bavsic type inferance. Anything after the "in" keyword is assumed to be an internal set, unquoted alphas are user supplied parameter and quoted alphas are literal values.
2. A set is a collection of unique elements to check against. betaTesters is a list of users I want to enable my new home page for, but only if they're in the UK. QA is a list of users in my QA department, they get all the buggy new features.
3. In my site code I wrap the new home page code in a call to SSP, supplying as many of the parameters as I can. It's optimistic so for my example if you're in QA or not on the beta testers list SSP doesn't need to be told the country, it'll return a true or false as soon as it's sure of the answer. If any of the parameters are required tho it'll reply with a list of missing parameters.
4. You get to see my new home page. Or not.

To add a new feature with a constraint:

    curl "ssp/feature" --data-binary '{"name": "NewHomePage", "constraint": "userId in betaTesters"}'

To create a beta tester list (This will change soon, a set can support 4 billion entries and should probably be a collection of it's own...):

    curl "ssp/list" --data-binary '{"name": "betaTesters", "members": ["tom"]}'

To check some parameters against a feature, or perhaps multiple features:

    curl "ssp/check/NewHomePage?userId=tom" ; echo
    curl "ssp/check/?feature=NewHomePage&feature=AnnoyingAdvertising&userId=tom" ; echo


HTTP is great for testing but for something like this it's not the quickest protocol. In the above examples less than 20% of the payload transfered on the wire is actually required data. There isn't one yet but in the future the query will throw off the shackles of HTTP and roam free and faster. Of course HTTP queries will always be there for testing/debugging and people who like running lots of servers.

### Installation

You'll need Python >= 2.7 but < 3, or even better PyPy, with [Cyclone](http://cyclone.io/) and a local Redis instance. Just run the start.sh script.