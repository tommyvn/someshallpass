#!/usr/bin/env python
# coding: utf-8
#
# Copyright 2013 Tom van Neerijnen <tom@tomvn.com>
# Powered by cyclone
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import cyclone.escape
import cyclone.web

from twisted.python import log

from someshallpass.storage import DatabaseMixin

from twisted.internet import defer


class BaseHandler(cyclone.web.RequestHandler):
    def get_user_locale(self):
        lang = self.get_secure_cookie("lang")
        if lang:
            return cyclone.locale.get(lang)

class BaseAPIHandler(cyclone.web.RequestHandler, DatabaseMixin):
    def prepare(self, feature_id=None):
        if not self.request.method != "OPTIONS":
            self.set_header("Access-Control-Allow-Headers", "X-Requested-With, X-HTTP-Method-Ovrride, Content-Type, Accept, Api-key, Api-secret")
            self.set_header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS")
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Content-Type", "application/json")
        self.account_id = "tom"

    # @defer.inlineCallbacks
    # def prepare(self, feature_id=None):
    #     if self.request.method != "OPTIONS":
    #         api_key = self.request.headers.get("Api-Key")
    #         api_secret = self.request.headers.get("Api-Secret")
    #         if not api_key:
    #             raise cyclone.web.HTTPError(401)
    #         credentials = yield self.redis.hgetall("api_key:{}".format(api_key))            
    #         self.account_id = credentials["account_id"]
    #     else:
    #         self.set_header("Access-Control-Allow-Headers", "X-Requested-With, X-HTTP-Method-Ovrride, Content-Type, Accept, Api-key, Api-secret")
    #         self.set_header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS")
    #     self.set_header("Access-Control-Allow-Origin", "*")
    #     self.set_header("Content-Type", "application/json")
    def options(self, feature_id=None):
        self.write("OK")

