#!/usr/bin/env python
# coding: utf-8
#
# Copyright 2013 Tom van Neerijnen <tom@tomvn.com>
# Powered by cyclone
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

try:
    sqlite_ok = True
    import cyclone.sqlite
except ImportError, sqlite_err:
    sqlite_ok = False

import cyclone.redis
import cyclone.escape

from twisted.enterprise import adbapi
from twisted.python import log

from twisted.internet import reactor
from twisted.internet import defer
import uuid
@defer.inlineCallbacks
def initialize_data(redis_conn):
    try:
        yield redis_conn.flushdb()

        account_id = "tom"
        redis_conn.hmset("api_key:tommysapikey", {"account_id": account_id, "api_secret": "tommysapisecret"})
        #set up features
        redis_conn.sadd("account:{}:features".format(account_id), "whitelist", "beta_tester", "test")
        # redis_conn.set("account:{}:feature:{}".format(account_id, "whitelist"), cyclone.escape.json_encode({"and": ["userId"]}))
        redis_conn.set("account:{}:feature:{}".format(account_id, "beta_tester"), cyclone.escape.json_encode({'AND': [ 'country', {'OR': [ 'bossian', 'reporter' ] } ]}))
        redis_conn.set("account:{}:feature:{}".format(account_id, "whitelist"), cyclone.escape.json_encode({"AND":["userId","bossian"]}))
        redis_conn.set("account:{}:feature:{}".format(account_id, "test"), cyclone.escape.json_encode({"AND":["country"]}))
        #set up lists
        redis_conn.hmset("account:{}:lists".format(account_id), {"bossian": "userId", "reporter": "userId", "allowedCountry": "country", "country": "country"})
        redis_conn.sadd("account:{}:list:{}".format(account_id, "bossian"), "tom", "otherguy")
        redis_conn.sadd("account:{}:list:{}".format(account_id, "reporter"), "456", "123")
        redis_conn.sadd("account:{}:list:{}".format(account_id, "allowedCountry"), "UK", "NZ")
        redis_conn.sadd("account:{}:list:{}".format(account_id, "country"), "UK", "NZ")

    except Exception, e:
        log.msg("redis not ready, retry initialization - {}".format(e))
        reactor.callLater(1, initialize_data, redis_conn)

class DatabaseMixin(object):
    mysql = None
    redis = None
    sqlite = None

    @classmethod
    @defer.inlineCallbacks
    def inline_setup(cls, conf):
        if "redis_settings" in conf:
            DatabaseMixin.redis = \
            yield cyclone.redis.Connection(
                            conf["redis_settings"].host,
                            conf["redis_settings"].port,
                            conf["redis_settings"].dbid)
            # yield initialize_data(DatabaseMixin.redis)
    @classmethod
    def setup(cls, conf):
        if "redis_settings" in conf:
            if conf["redis_settings"].get("unixsocket"):
                DatabaseMixin.redis = \
                cyclone.redis.lazyUnixConnectionPool(
                              conf["redis_settings"].unixsocket,
                              conf["redis_settings"].dbid,
                              conf["redis_settings"].poolsize)
            else:
                DatabaseMixin.redis = \
                cyclone.redis.lazyConnectionPool(
                              conf["redis_settings"].host,
                              conf["redis_settings"].port,
                              conf["redis_settings"].dbid,
                              conf["redis_settings"].poolsize)
            # initialize_data(DatabaseMixin.redis)
