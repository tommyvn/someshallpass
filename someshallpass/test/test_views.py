from someshallpass.storage import DatabaseMixin
from someshallpass.views import CheckFeature
from twisted.trial import unittest
from someshallpass import config
from twisted.internet import defer
from twisted.python import log

config_file = "/home/tom/git/bossa/server_someshallpass/someshallpass.conf"


# class CheckFeatureTestCase(unittest.TestCase):
#     @defer.inlineCallbacks
#     def setUp(self):
#         conf = config.parse_config(config_file)
#         yield DatabaseMixin.inline_setup(conf)
#         self.checkFeature = CheckFeature()
#         self.checkFeature.account_id = "tom"

#     @defer.inlineCallbacks
#     def test_check_feature(self):
#         parameter = { "country": "UK", "bossian": "456", "reporter": "456" }
#         test = yield self.checkFeature.check_feature("beta_tester", parameter)
#         self.assertEquals(test, {"result": True})
#         log.msg(test)
#         self.assertEquals(1,1)

#     @defer.inlineCallbacks
#     def tearDown(self):
#         yield self.checkFeature.redis.disconnect()
