#!/usr/bin/env python
# coding: utf-8
#
# Copyright 2013 Tom van Neerijnen <tom@tomvn.com>
# Powered by cyclone
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import cyclone.escape
import cyclone.locale
import cyclone.web

from twisted.internet import defer
from twisted.python import log

from someshallpass.storage import DatabaseMixin
from someshallpass.utils import BaseHandler, BaseAPIHandler
#from someshallpass.utils import TemplateFields

import uuid

# from dsltolua import Expression
import dsltolua
# from querylanguage.QueryLanguage import Expression

class IndexHandler(BaseHandler):
    def get(self):
        self.render("index.html")

class ListsHandler(BaseAPIHandler):
    @defer.inlineCallbacks
    def get(self):
        lists = yield self.redis.smembers("account:{}:lists".format(self.account_id))
        self.write({ "data": [ {"id": lid, "name": lid} for lid in lists ]})
    def post(self):
        list_details = cyclone.escape.json_decode(self.request.body)
        list_id = list_details['name']
        self.redis.sadd("account:{}:lists".format(self.account_id), list_id)
        self.redis.delete("account:{}:list:{}".format(self.account_id, list_id))
        self.redis.sadd("account:{}:list:{}".format(self.account_id, list_id), list_details['members'])
        self.write(list_details)

class ListHandler(BaseAPIHandler):
    @defer.inlineCallbacks
    def get(self, list_id):
        members = yield self.redis.smembers("account:{}:list:{}".format(self.account_id, list_id))
        self.write({"name": list_id, "id": list_id, "members": list(members)})
    @defer.inlineCallbacks
    def put(self, list_id):
        list_details = cyclone.escape.json_decode(self.request.body)
        yield self.redis.delete("account:{}:list:{}".format(self.account_id, list_id))
        self.redis.sadd("account:{}:list:{}".format(self.account_id, list_id), list_details['members'])
        self.write(list_details)
    def delete(self, list_id):
        self.redis.srem("account:{}:lists".format(self.account_id), list_id)
        self.redis.delete("account:{}:list:{}".format(self.account_id, list_id))
        self.set_status(204)
        self.finish()

class FeaturesHandler(BaseAPIHandler):
    @defer.inlineCallbacks
    def get(self):
        features = yield self.redis.smembers("account:{}:features".format(self.account_id))
        self.write({ "data": [ {"id": lid, "name": lid} for lid in features ] })
    @defer.inlineCallbacks
    def post(self):
        feature = cyclone.escape.json_decode(self.request.body)
        feature_id = feature['name']

        thing = dsltolua.Expression()
        dbprefix = "account:tom:list:"
        try:
            parsed = thing.parse(feature['constraint'])
        except:
            raise cyclone.web.HTTPError(500)
        lua_script = dsltolua.lua_base.format(parsed.eval(dbprefix))
        evalsha = yield self.redis.script_load(lua_script)
        keys_required = list(parsed.keys_required(dbprefix))
        self.redis.hmset("account:{}:feature:{}".format(self.account_id, feature_id), {"constraint": feature['constraint'], "evalsha": evalsha, "keys_required": cyclone.escape.json_encode(keys_required)})
        yield self.redis.delete("account:{}:feature:{}:parameters".format(self.account_id, feature_id))
        yield self.redis.sadd("account:{}:feature:{}:parameters".format(self.account_id, feature_id), list(parsed.parameters_required(dbprefix)))
        self.redis.sadd("account:{}:features".format(self.account_id), feature_id)
        
        self.write(feature)

class FeatureHandler(BaseAPIHandler):
    @defer.inlineCallbacks
    def get(self, feature_id):
        feature = yield self.redis.hgetall("account:{}:feature:{}".format(self.account_id, feature_id))
        constraint = feature["constraint"]
        self.write({"name": feature_id, "id": feature_id, "constraint": constraint})
    @defer.inlineCallbacks
    def put(self, feature_id):
        feature = cyclone.escape.json_decode(self.request.body)

        thing = dsltolua.Expression()
        dbprefix = "account:tom:list:"
        try:
            parsed = thing.parse(feature['constraint'])
        except:
            raise cyclone.web.HTTPError(500)
        lua_script = dsltolua.lua_base.format(parsed.eval(dbprefix))
        evalsha = yield self.redis.script_load(lua_script)
        keys_required = list(parsed.keys_required(dbprefix))
        self.redis.hmset("account:{}:feature:{}".format(self.account_id, feature_id), {"constraint": feature['constraint'], "evalsha": evalsha, "keys_required": cyclone.escape.json_encode(keys_required)})
        yield self.redis.delete("account:{}:feature:{}:parameters".format(self.account_id, feature_id))
        required_parameters = list(parsed.parameters_required(dbprefix))
        if required_parameters:
            yield self.redis.sadd("account:{}:feature:{}:parameters".format(self.account_id, feature_id), required_parameters)

        self.write(feature)
    def delete(self, feature_id):
        self.redis.srem("account:{}:features".format(self.account_id), feature_id)
        self.redis.delete("account:{}:feature:{}".format(self.account_id, feature_id))
        self.set_status(204)
        self.finish()

class CheckFeature(DatabaseMixin):
    @defer.inlineCallbacks
    def check_feature(self, feature_id, parameters):
        if parameters:
            param_key = "parametersInFlight:{}".format(str(uuid.uuid4()))
            encoded_parameters = {k: cyclone.escape.json_encode(v) for k, v in parameters.items()}
            yield self.redis.hmset(param_key, encoded_parameters)
            self.redis.expire(param_key, 5)
        else:
            param_key = None
        feature_details = yield self.redis.hgetall("account:{}:feature:{}".format(self.account_id, feature_id))
        evalsha = feature_details["evalsha"]
        required_keys = cyclone.escape.json_decode(feature_details["keys_required"])
        try:
            result = yield self.redis.evalsha(evalsha, [param_key] + required_keys)
        except cyclone.redis.ScriptDoesNotExist:
            thing = dsltolua.Expression()
            dbprefix = "account:tom:list:"
            try:
                parsed = thing.parse(feature_details['constraint'])
            except:
                raise cyclone.web.HTTPError(500)
            lua_script = dsltolua.lua_base.format(parsed.eval(dbprefix))
            evalsha = yield self.redis.script_load(lua_script)
            if evalsha != feature_details["evalsha"]:
                raise cyclone.web.HTTPError(500)
            defer.returnValue((yield self.check_feature(feature_id, parameters)))
        if result[0]:
            defer.returnValue({ "result": bool(result[1])})
        else:
            required_parameters = yield self.redis.smembers("account:{}:feature:{}:parameters".format(self.account_id, feature_id))
            defer.returnValue({ "missing": list(required_parameters.difference(set(parameters.keys())))})

class CheckHandler(BaseAPIHandler, CheckFeature):
    @defer.inlineCallbacks
    def get(self, feature_id):
        result = yield self.check_feature(feature_id, { k: v[0] for k, v in self.request.arguments.items() })
        self.write(result)


class ChecksHandler(BaseAPIHandler, CheckFeature):
    @defer.inlineCallbacks
    def get(self):
        features = self.request.arguments.pop("feature")
        parameters = { k: v[0] for k, v in self.request.arguments.items() }
        result = {}
        for feature in features:
            result[feature] = yield self.check_feature(feature, parameters)
        self.write(result)
