from someshallpass.dsltolua import Expression
from twisted.trial import unittest


class QueryLanguageTest(unittest.TestCase):
    def setUp(self):
        self._expression = Expression()
        self.dbprefix = "account:tom:feature:"
    def parse_and_eval(self, QL):
        parsed = self._expression.parse(QL)
        return parsed.eval(self.dbprefix)

    def parse_and_check_required_keys(self, QL):
        parsed = self._expression.parse(QL)
        return parsed.keys_required(self.dbprefix)

    def parse_and_check_required_parameters(self, QL):
        parsed = self._expression.parse(QL)
        return parsed.parameters_required(self.dbprefix)

    def test_parameter_equal_string(self):
        QL = '''country == "UK"'''

        LUA = "(pget('country') == 'UK')".format(self.dbprefix)

        self.assertEqual(self.parse_and_eval(QL), LUA)


    def test_parameter_equal_number(self):
        QL = "age == 18"

        LUA = "(pget('age') == 18)".format(self.dbprefix)

        self.assertEqual(self.parse_and_eval(QL), LUA)

    def test_parameter_in_set(self):
        QL = "userId in bossian"

        LUA = "redis.call('SISMEMBER', '{}bossian', pget('userId')) == 1".format(self.dbprefix)

        self.assertEqual(self.parse_and_eval(QL), LUA)

    def test_complexish_query1(self):
        QL = """thing in someset and 3 <= 2"""

        LUA = "(redis.call('SISMEMBER', 'account:tom:feature:someset', pget('thing')) == 1 and (3 <= 2))"

        self.assertEqual(self.parse_and_eval(QL), LUA)

    def test_complexish_query1_required_params(self):
        QL = """thing in someset and 3 <= 2"""

        REQUIRED_PARAMETERS = set(["thing"])

        self.assertEqual(self.parse_and_check_required_parameters(QL), REQUIRED_PARAMETERS)    

    def test_complexish_query2(self):
        QL = """userId in bossian and ( userId == "jim" or ( jim in allowedUser and userage >= 18 ) )"""

        LUA = "(redis.call('SISMEMBER', 'account:tom:feature:bossian', pget('userId')) == 1 and ((pget('userId') == 'jim') or (redis.call('SISMEMBER', 'account:tom:feature:allowedUser', pget('jim')) == 1 and (pget('userage') >= 18))))".format(self.dbprefix)

        self.assertEqual(self.parse_and_eval(QL), LUA)

    def test_2_required_keys(self):
        QL = "userId in bossian and ( userId in something or ( jim in allowedUser and userCountry in allowedCountry ) )"

        REQUIRED_KEYS = set(["account:tom:feature:bossian", "account:tom:feature:allowedCountry", "account:tom:feature:something", "account:tom:feature:allowedUser"])

        self.assertEqual(self.parse_and_check_required_keys(QL), REQUIRED_KEYS)

    def test_1_required_key(self):
        QL = "userId in bossian"

        REQUIRED_KEYS = set(["account:tom:feature:bossian",])

        self.assertEqual(self.parse_and_check_required_keys(QL), REQUIRED_KEYS)

    def test_params_required(self):
        QL = """(userid == "healthcheck" and platform == "healthcheck") or (playerid == "healthcheck" and (application == "healthcheck" or game == "healthcheck"))"""

        REQUIRED_PARAMETERS = set(["userid", "platform", "playerid", "application", "game"])

        self.assertEqual(self.parse_and_check_required_parameters(QL), REQUIRED_PARAMETERS)