#!/usr/bin/env python
# coding: utf-8
#
# Copyright 2013 Tom van Neerijnen <tom@tomvn.com>
# Powered by cyclone
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from pyparsing import *

class MissingParameterError(Exception):
    def __init__(self, value):
        self.value = value

class BaseOp(object):
    def __init__(self,t):
        self.tokens = t[0][0::2]
        self.operation = t[0][1]
    def __str__(self):
        return "({})".format(" {} ".format(self.operation).join(map(str, self.tokens)))
    # There's something very, very wrong here, but it works and only delays updating the constraint, not processing it. I'll look into it later.
    def keys_required(self, dbprefix):
        kr = set()
        for t in self.tokens:
            try:
                ikr = t.keys_required(dbprefix)
            except AttributeError:
                continue
            if ikr:
                kr.update(ikr)
        return kr
    # see keys_required comment, this is working but not like it should.
    def parameters_required(self, dbprefix):
        kr = set()
        for t in self.tokens:
            try:
                ikr = t.parameters_required(dbprefix)
            # except AttributeError:
            except TypeError:
                continue
            if ikr:
                kr.update(ikr)
        return kr
    def eval(self, dbprefix):
        return "({})".format(" {} ".format(self.operation).join([ t.eval(dbprefix) for t in self.tokens ]))

class AndOp(BaseOp):
    pass

class OrOp(BaseOp):
    pass

class NotOp(object):
    def __init__(self,tokens):
        self.token = tokens[0][1]
    def __str__(self):
        return "!{}".format(self.token)
    def eval(self, dbprefix):
        return "not({})".format(self.token.eval(dbprefix))

class EvalComparisonOp(BaseOp):
    pass

class BoolInSSPSet(BaseOp):
    def __init__(self,t):
        self.tokens = t[0][0::2]
        self.operation = t[0][1]
        (self.member_to_check, set_list_to_check) = self.tokens
        self.set_to_check = set_list_to_check[0]
    def eval(self, dbprefix):
        return """redis.call('SISMEMBER', '{}{}', {}) == 1""".format(dbprefix, self.set_to_check, self.member_to_check.eval(dbprefix))
    def keys_required(self, dbprefix):
        return set(["{}{}".format(dbprefix, self.set_to_check)])

class EvalConstantOrVariable(object):
    def __init__(self, token):
        self.value = token[0][0]
        self.type = token.getName()
    def __str__(self):
        return str("{}:{}".format(self.type, self.value))
    def eval(self, dbprefix):
        if self.type == "number":
            return self.value
        elif self.type == "variable":
            return """pget('{}')""".format(self.value)
        elif self.type == "literal":
            return """'{}'""".format(self.value)
        # If I could make this work I'd be living the dream.
        elif self.type == "set":
            return """SET!!!!! {}{}""".format(dbprefix, self.value)
    def parameters_required(self, dbprefix):
        if self.type == "variable":
            return set([self.value])