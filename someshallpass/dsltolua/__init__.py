#!/usr/bin/env python
# coding: utf-8
#
# Copyright 2013 Tom van Neerijnen <tom@tomvn.com>
# Powered by cyclone
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from Operands import *

lua_base = """
local missing = {{}}
local pget = function(pname)
  local r = redis.call('hget', KEYS[1], pname)
  if not r then
    table.insert(missing, pname)
    return nil
  end
  return cjson.decode(r)
end

local result = nil

if {} then
  result = 1
else
  result = 0
end

if next(missing) == nil then
  return {{true,result}}
else
  return {{false,missing}}
end
"""

class Expression(object):


    def __init__(self):

        literal = Group(Suppress('"') + Word(alphanums) + Suppress('"')).setResultsName("literal")
        # literal.setResultsName("literal")
        variable = Group(Word(alphanums)).setResultsName("variable")
        integer = Group(Word(nums + '-')).setResultsName("number") # This will soon become numbers

        # # comparison
        # variable = oneOf("l p s f") + ":" + Word(alphanums)
        # integer = Keyword("l") + ":" + Word(nums + '-')

        comparison_operator = oneOf("< <= > >= == != <>")

        # sets
        # set_operator = oneOf("in") + White() + variable
        set_operator = oneOf("in") + Suppress(White()) + variable

        # operand = integer | variable
        operand = integer | literal | variable
        # operand = variable
        operand.setParseAction(EvalConstantOrVariable)
        self.expr = operatorPrecedence(operand,
            [
                # (set_operator, 2, opAssoc.LEFT, BoolInSSPSet),
                (set_operator, 1, opAssoc.LEFT, BoolInSSPSet),
                (comparison_operator, 2, opAssoc.LEFT, EvalComparisonOp),
                ("not", 1, opAssoc.RIGHT,  NotOp),
                ("and", 2, opAssoc.LEFT,  AndOp),
                ("or", 2, opAssoc.LEFT,  OrOp),
            ])

    def parse(self, dsl_expression):
        return self.expr.parseString(dsl_expression)[0]

    def to_lua(self):
        pass